const Esper = require('./api-core/esper-api.js');
let esper = new Esper();

//require interactive handler
let interactive = require('./api-core/esper-interactive.js');

//register our start and stop events
interactive.register("b","begin MV capture",()=>{esper.startMvDrivenCapture()});
interactive.register("s","stop MV capture",()=>{esper.stopMVDrivenCapture()});

//this script allows you to control the input gating for the MV cameras.
//if the MV cameras are outputting their sync signal, this script allows you to start and stop an mv capture, provided a
// suitable lighting has been uploaded to the lights



//connect to the api and
esper.connect() //connect to the ws sever as usual
    .then(()=>{
        //start interactive mode - it provides a text summary of available commands to the user
        interactive.start();
    });



