const Esper = require('./api-core/esper-api.js');
const {create8GradientStages} = require("./create8GradientStages.js");
let esper = new Esper();

//config for the script below
let dslrFlashDuration = 5;
let mvFlashDuration = 0.3;

//places to store the various sequences and stages
let mvLightingStage = [];

//connect to the api and
esper.connect()
    .then(()=>{
        return create8GradientStages(esper, dslrFlashDuration);
    })
    .then((sequenceData)=> {
        let dslrSequenceLength = sequenceData.length;


        //sequence data contains the 8 gradient stages for the dslr capture

        //create a single lighting stage for the mv that will be repeated ad-infinitum
        for (let light of esper.availableLights){
            let lsp = {
                id:light.id,
                duration: mvFlashDuration,
                intensities: [100,0,0]
            };
            mvLightingStage.push(lsp);
        }


        //spam in a bunch of mv stages
        sequenceData.push(mvLightingStage);
        sequenceData.push(mvLightingStage);
        sequenceData.push(mvLightingStage);
        sequenceData.push(mvLightingStage);
        sequenceData.push(mvLightingStage);

        //upload the sequence to the dome
        esper.sequence(sequenceData).catch((err)=>{console.table(err)});

        //work out the loop at and loop to stages for the mv - based off the position of the end of the dslr sequence in the array
        let loopTo = dslrSequenceLength  + 1;
        let loopAt = loopTo + 3;

        //configure the bits of the lighting stage to loop at
        esper.setLoopAtStage(loopAt);
        esper.setLoopToStage(loopTo);
        esper.setCurrentSequencePoint(loopTo);
    });


