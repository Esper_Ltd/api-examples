let EsperFactory = require("./esper-api.js");
let esper = new EsperFactory();

esper.connect().then(()=>{
    return esper.getAvailableLights();
})
.then((availableLights)=>{
    console.table(availableLights);
})
    .then(()=>{
        esper.disconnect();
    });
