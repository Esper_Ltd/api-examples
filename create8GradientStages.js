const saturateToExtrema = true;

// define the lighting directions  (change to match your direction convention if needed..)
const DIR_VECT_GLOBAL    = [ 0, 0, 0];
const DIR_VECT_SUB_LEFT  = [ 1, 0, 0];
const DIR_VECT_SUB_RIGHT = [-1, 0, 0];
const DIR_VECT_SUB_TOP   = [ 0, 1, 0];
const DIR_VECT_SUB_BOT   = [ 0,-1, 0];
const DIR_VECT_SUB_FRONT = [ 0, 0, 1];
const DIR_VECT_SUB_BACK  = [ 0, 0,-1];

// define led names: (don't change!)
const NEUTRAL_LED    = 1;
const CROSS_LED      = 0;
const PARALLEL_LED   = 2;


// change this bit to allow  you to configure the order of lighting states to match your lightstages.json file

// specify which directions to upload in sequence order: (This array length must match ledsToUse.length)
let directionsToUse = [
    DIR_VECT_GLOBAL,
    DIR_VECT_GLOBAL,
    DIR_VECT_GLOBAL,
    DIR_VECT_SUB_LEFT,
    DIR_VECT_SUB_RIGHT,
    DIR_VECT_SUB_TOP,
    DIR_VECT_SUB_BOT,
    DIR_VECT_SUB_FRONT,
];

// specify which leds to use in sequence order:
const ledsToUse = [
    NEUTRAL_LED,            //gi
    CROSS_LED,              //gi
    PARALLEL_LED,           //gi
    CROSS_LED,              //gradients onwards as per L22 onwards
    CROSS_LED,
    CROSS_LED,
    CROSS_LED,
    CROSS_LED,
];


//compensate for differing brightnesses of each lighting state
const perStageIntensityMask = [
    0.4,
    1,
    0.7,
    1,
    1,
    1,
    1,
    1,
];



let create8GradientStages = (apiWrapper, flashDuration)=>{
    if (ledsToUse.length === directionsToUse.length && ledsToUse.length === perStageIntensityMask.length){
    let lights = JSON.parse(JSON.stringify(apiWrapper.availableLights)); // lazy deep copy.
    for (let light of lights){
        light.intensitiesTable = [];
        light.ledsTable = [];
        for (let sIt = 0; sIt < directionsToUse.length; sIt++){
            let directionVector = directionsToUse[sIt];
            let calculatedIntensity_Unitary = -1;
            if (calcMagnitude(directionVector) > 0.001){ // if vector is [0,0,0], light from everywhere.
                calculatedIntensity_Unitary = calcUnitaryInteractionWith(light.positions, directionVector);
                if (saturateToExtrema === true){
                    calculatedIntensity_Unitary = (calculatedIntensity_Unitary > 0.95) ? 1 : 0;
                }
            }
            else{
                calculatedIntensity_Unitary = 1; // GI
            }
            light.intensitiesTable.push(calculatedIntensity_Unitary * 100 * perStageIntensityMask[sIt]);
            light.ledsTable.push(ledsToUse[sIt]);
        }
    }

    let sequenceHolder = [];
    for (let stageIndex = 0; stageIndex < directionsToUse.length; stageIndex++){
        let thisStageArray = [];
        for (let light of lights){
            let intensitiesTrio = [0,0,0];
            intensitiesTrio[light.ledsTable[stageIndex]] = light.intensitiesTable[stageIndex];
            thisStageArray.push({
                id: light.id,
                intensities: intensitiesTrio,
                duration: flashDuration
            })
        }
        sequenceHolder.push(thisStageArray);
    }
    return sequenceHolder;
    }else {
        return Promise.reject("script configuration issue - length of directions, ledsToUse or intensityMask dont all match");
    }
};


function dotProduct(a, b){
    if (a !== null && b !== null){
        return (a[0] * b[0]) + (a[1] * b[1]) + (a[2] * b[2]);
    }
    return null;
}



/**
 *
 * @param {number[]} vect1
 * @param {number[]} vect2
 * @returns {null|number}
 */
function calcUnitaryInteractionWith(vect1, vect2) {
    if (Array.isArray(vect1) && Array.isArray(vect2)){
        let dot = dotProduct(vect1, vect2);
        let magThis = calcMagnitude(vect1);
        let magThat = calcMagnitude(vect2);
        let unityInteraction = dot / (magThis * magThat);
        unityInteraction += 1;
        unityInteraction /= 2;
        if (!(unityInteraction >= 0 && unityInteraction <= 1)) {
            console.error("Unitary interaction calculation error:" + unityInteraction);
            return null;
        }
        return unityInteraction;
    }
    else{
        console.error("Typerror on calcUnitaryInteractionWith()");
        return null;
    }
}

/**
 *
 * @param {number[]} vectIn
 * @returns {number}
 */
function calcMagnitude(vectIn){
    return Math.sqrt(
        Math.pow(vectIn[0],2) +
        Math.pow(vectIn[1],2) +
        Math.pow(vectIn[2],2)
    );
}


module.exports = {create8GradientStages};