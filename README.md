# Prerequisites:
- node interpreter installed
- npm installed
- git installed

#Installation
Esper's `api-core` repo is submoduled into this repo. the `install.sh` script takes care of installing the `api-core`.
It makes sure the submodule is updated, it then `cd` into the `api-core` directory, issues an `npm install` command to install the packages that `api-core` depends on then returns back to the repo root. 

# Usage
Functionality has been split for a better experience when using these scripts. One script is responsible for uploading the required data to the dome, then another script is responsible for running the capture

### OLAT
to run an MV driven OLAT capture, first run the `upload-OLAT-data.esper.js`, this uploads the OLAT data and sets the lights into the correct mode for OLAT
then, run the `start-stop-MVCapture.esper.js` script to control the starting and stopping of the continuous OLAT capture

### Interleaved mode
First, run the `upload-interleaved-sequence.esper.js` script, this programs the lights with the 8 stages for the DLSRs and a bunch of GI stages for the MV cameras. 
It then sets the `loopAt` and `loopTo` stages to loop over the repeated GI stages, then it sets the `currentSequencePoint` to the start of the GI stages so when the lights are first triggered by the MV cameras, they are inside the looped GI to begin with


After uploading the interleaved data, run the `runInterleavedCapture.esper.js` script, this allows you to press `b` to begin MV capture, press `s` to stop MV capture and press `space` to insert a DSLR capture, returning to continuous MV GI after the dslrs have finished

# Gradients
You can create lighting gradients for use in your api scripts with the function exported in `create8GradientStages.js`

```javascript
//include the api wrapper
const Esper = require('./api-core/esper-api.js');

//instantiate
let esper = new Esper();

//require the gradient creating function
const {create8GradientStages} = require("./create8GradientStages.js");

//specify what flash duration you want
let flashDuration = 5.5; // milliseconds


/* 
    because of async stuff, this function returns a promise that resolves with 
    the gradient lighting profiles you require
*/
create8GradientStages(esper, flashDuration) 
    .then((sequenceData)=> { //sequenceData is an array of lighting stages that correspond to the required gradients
        //you can upload them directly like so
        return esper.sequence(sequenceData);
    })  
    .then(()=>{
        //do something else - maybe esper.trigger() 
    })  
```

It is worth checking out the function in `create8GradientStages.js` - its pretty comprehensively commented, it shouldnt be too hard to tweak it to your liking 