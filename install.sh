#!/bin/bash
git submodule update --init --recursive
cd api-core || echo "could not cd into api-core directory - please check this directory is present in the root of this project repo"
npm install || echo "npm install command failed - the above output may tell you what is wrong"
echo ""
echo "installer finished"
cd ../