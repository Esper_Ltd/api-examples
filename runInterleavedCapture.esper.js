const Esper = require('./api-core/esper-api');
let interactive = require('./api-core/esper-interactive.js');
let esper = new Esper();


interactive.register("b","begin MV capture",()=>{esper.startMvDrivenCapture()});
interactive.register("s","stop MV capture",()=>{esper.stopMVDrivenCapture()});
interactive.register(" ","begin MV capture",()=> {
    esper.stopMVDrivenCapture();
    esper.trigger({
        start: 0,
        stages: 8,
        fps: 10,
        preFocusDelay: 250
    })
        .then(() => {
            esper.startMvDrivenCapture();
        });
});



esper.connect()
.then(()=> {
    console.log("Interleaved capture - Make sure you have already uploaded your lighting data to the dome");
    interactive.start();
});