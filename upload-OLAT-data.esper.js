const EsperFactory = require('./api-core/esper-api.js');
let esper = new EsperFactory();

/*

This script has 5 steps:
    1) Connects to the API Websocket server
    2) creates a "chain mode" lighting payload
    3) uploads the "chain mode" payload to the dome. this specifies the order the lights should flash in and for how long
    4) arms chain mode - this is where the LED brightnesses are specified.
    5) it then tells you the rig is ready for triggering
 */


//configuration:
let flashDuration = 5;  // min: 1, max: 30;
let intensities = [100,0,0]; //percent brightness for each of the three leds


esper.connect() //call connect method then create a promise chain to do all the async stuff
    .then(() => {
        // all the available lights are enumerated in the array esper.availableLights

        //read the lights into a local variable, then break the pass-by-reference with stringify->parse
        //we dont strictly need to do this, but if you had a script where you want to mutate the available lights,
        // this method of reading into a local variable and breaking pass-by-reference is best practice
        let lights = JSON.parse(JSON.stringify(esper.availableLights));

        //provide some user feedback
        console.log("generating OLAT payload for "+ esper.availableLights.length + " lights")

        //Generate the payload
        let olatPayload = [];

        //we want to make an array of objects, one object per light, that tell the light what to do for chain mode
        //in this case, we want light #1 to flash on the first trigger, light #2 to flash on the second trigger, light #37
        // to flash on the 37th trigger and so on for every light. the flash position is how many triggers that light should
        // wait for before flashing
        for(let i=0;i<lights.length;i++){
            olatPayload.push({
                id: lights[i].id,               //which light
                flashPosition: i,               //how long to wait before flashing
                duration: flashDuration,    //how long to flash for
            });
        }

        console.table(olatPayload);
        esper.configureChainMode(olatPayload)
            .then(()=>{
                return esper.setLoopAtStage(lights.length);     //loop once all the lights have flashed once
            })
            .then(()=>{
                return esper.setLoopToStage(0);     //loop back to the beginning
            })
            .then(()=>{
                return esper.armChainMode(intensities);
            })
            .then(()=>{
                console.log("armed... ready for trigger.");
               //this script is effectively done
            })
            .catch((err)=>{
                console.table(err);
            });
    });
